<?php
############ Configuration ##############
$destination_folder		= '../img/'; //upload directory ends with / (slash)
##########################################

//continue only if $_POST is set and it is a Ajax request
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){


	$whichimage = $_POST["whichimage"];

	// check $_FILES['ImageFile'] not empty
	if(!isset($_FILES['image_file']) || !is_uploaded_file($_FILES['image_file']['tmp_name'])){
			die('Image file is Missing!'); // output error when above checks fail.
	}

	//uploaded file info we need to proceed
	$image_name = $_FILES['image_file']['name']; //file name
	$image_temp = $_FILES['image_file']['tmp_name']; //file temp

	$image_size_info 	= getimagesize($image_temp); //get image size

	if($image_size_info){
		$image_type 		= $image_size_info['mime']; //image type
	}else{
		die("Make sure image file is valid!");
	}

	//switch statement below checks allowed image type
	//as well as creates new image from given file
	switch($image_type){
		case 'image/png':
			$image_res =  imagecreatefrompng($image_temp); imagealphablending($image_res, false);
			imagesavealpha($image_res, true); break;
		case 'image/gif':
			$image_res =  imagecreatefromgif($image_temp); break;
		case 'image/jpeg': case 'image/pjpeg':
			$image_res = imagecreatefromjpeg($image_temp); break;
		default:
			$image_res = false;
	}

	if($image_res){
		//Get file extension and name to construct new file name
		$image_info = pathinfo($image_name);
		$image_extension = strtolower($image_info["extension"]); //image extension
		$image_name_only = strtolower($image_info["filename"]);//file name only, no extension

		//create a name for new image (Eg: fileName_293749.jpg) ;
		$new_file_name = $whichimage . '.' . $image_extension;

		//folder path to save resized images and thumbnails

		$image_save_folder 	= $destination_folder . $new_file_name;

		//call normal_resize_image() function to proportionally resize image
		if(normal_resize_image($image_res, $image_save_folder, $image_type))
		{

		}

		imagedestroy($image_res); //freeup memory
	}
}

#####  This function will proportionally resize image #####
function normal_resize_image($source, $destination, $image_type){

	//do not resize if image is smaller than max size
		if(save_image($source, $destination, $image_type)){
			return true;
		}


}

##### Saves image resource to file #####
function save_image($source, $destination, $image_type){
	switch(strtolower($image_type)){//determine mime type
		case 'image/png':
			imagepng($source, $destination); return true; //save png file
			break;
		case 'image/gif':
			imagegif($source, $destination); return true; //save gif file
			break;
		case 'image/jpeg': case 'image/pjpeg':
			imagejpeg($source, $destination); return true; //save jpeg file
			break;
		default: return false;
	}
}

##### Convert everything to proper file type #####
$filePath1 = "../img/cover";
$filePath2 = "../img/circleA";
$filePath3 = "../img/circleB";
$filePath4 = "../img/circleC";
$filePath5 = "../img/circleD";
$filePath6 = "../img/menuBG";

$filePath7 = "../img/logo";

##### Make png logo transparent #####

##### Convert Png images to Jpeg #####
$image1P = imagecreatefrompng($filePath1 . ".png");
$image2P = imagecreatefrompng($filePath2 . ".png");
$image3P = imagecreatefrompng($filePath3 . ".png");
$image4P = imagecreatefrompng($filePath4 . ".png");
$image5P = imagecreatefrompng($filePath5 . ".png");
$image6P = imagecreatefrompng($filePath6 . ".png");
$bg1 = imagecreatetruecolor(imagesx($image1P), imagesy($image1P));
$bg2 = imagecreatetruecolor(imagesx($image2P), imagesy($image2P));
$bg3 = imagecreatetruecolor(imagesx($image3P), imagesy($image3P));
$bg4 = imagecreatetruecolor(imagesx($image4P), imagesy($image4P));
$bg5 = imagecreatetruecolor(imagesx($image5P), imagesy($image5P));
$bg6 = imagecreatetruecolor(imagesx($image6P), imagesy($image6P));
imagefill($bg1, 0, 0, imagecolorallocate($bg1, 255, 255, 255));
imagefill($bg2, 0, 0, imagecolorallocate($bg2, 255, 255, 255));
imagefill($bg3, 0, 0, imagecolorallocate($bg3, 255, 255, 255));
imagefill($bg4, 0, 0, imagecolorallocate($bg4, 255, 255, 255));
imagefill($bg5, 0, 0, imagecolorallocate($bg5, 255, 255, 255));
imagefill($bg6, 0, 0, imagecolorallocate($bg6, 255, 255, 255));
imagealphablending($bg1, TRUE);
imagealphablending($bg2, TRUE);
imagealphablending($bg3, TRUE);
imagealphablending($bg4, TRUE);
imagealphablending($bg5, TRUE);
imagealphablending($bg6, TRUE);
imagecopy($bg1, $image1P, 0, 0, 0, 0, imagesx($image1P), imagesy($image1P));
imagecopy($bg2, $image2P, 0, 0, 0, 0, imagesx($image2P), imagesy($image2P));
imagecopy($bg3, $image3P, 0, 0, 0, 0, imagesx($image3P), imagesy($image3P));
imagecopy($bg4, $image4P, 0, 0, 0, 0, imagesx($image4P), imagesy($image4P));
imagecopy($bg5, $image5P, 0, 0, 0, 0, imagesx($image5P), imagesy($image5P));
imagecopy($bg6, $image6P, 0, 0, 0, 0, imagesx($image6P), imagesy($image6P));
imagedestroy($image1P); imagedestroy($image2P); imagedestroy($image3P); imagedestroy($image4P); imagedestroy($image5P); imagedestroy($image6P);
$quality = 100; // 0 = worst / smaller file, 100 = better / bigger file
imagejpeg($bg1, $filePath1 . ".jpg", $quality);
imagejpeg($bg2, $filePath2 . ".jpg", $quality);
imagejpeg($bg3, $filePath3 . ".jpg", $quality);
imagejpeg($bg4, $filePath4 . ".jpg", $quality);
imagejpeg($bg5, $filePath5 . ".jpg", $quality);
imagejpeg($bg6, $filePath6 . ".jpg", $quality);
imagedestroy($bg1); imagedestroy($bg2); imagedestroy($bg3); imagedestroy($bg4); imagedestroy($bg5); imagedestroy($bg6);
unlink($filePath1 . ".png"); unlink($filePath2 . ".png"); unlink($filePath3 . ".png"); unlink($filePath4 . ".png"); unlink($filePath5 . ".png"); unlink($filePath6 . ".png");

##### Convert Gif images to Jpeg #####
$image1G = imagecreatefromgif($filePath1 . ".gif");
$image2G = imagecreatefromgif($filePath2 . ".gif");
$image3G = imagecreatefromgif($filePath3 . ".gif");
$image4G = imagecreatefromgif($filePath4 . ".gif");
$image5G = imagecreatefromgif($filePath5 . ".gif");
$image6G = imagecreatefromgif($filePath6 . ".gif");
imagejpeg($image1G, $filePath1 . ".jpg");
imagejpeg($image2G, $filePath2 . ".jpg");
imagejpeg($image3G, $filePath3 . ".jpg");
imagejpeg($image4G, $filePath4 . ".jpg");
imagejpeg($image5G, $filePath5 . ".jpg");
imagejpeg($image6G, $filePath6 . ".jpg");
unlink($filePath1 . ".gif"); unlink($filePath2 . ".gif"); unlink($filePath3 . ".gif"); unlink($filePath4 . ".gif"); unlink($filePath5 . ".gif"); unlink($filePath6 . ".gif");

##### Convert Jpeg to Jpg #####
$image1J = imagecreatefromjpeg($filePath1 . ".jpeg");
$image2J = imagecreatefromjpeg($filePath2 . ".jpeg");
$image3J = imagecreatefromjpeg($filePath3 . ".jpeg");
$image4J = imagecreatefromjpeg($filePath4 . ".jpeg");
$image5J = imagecreatefromjpeg($filePath5 . ".jpeg");
$image6J = imagecreatefromjpeg($filePath6 . ".jpeg");
imagejpeg($image1J, $filePath1 . ".jpg");
imagejpeg($image2J, $filePath2 . ".jpg");
imagejpeg($image3J, $filePath3 . ".jpg");
imagejpeg($image4J, $filePath4 . ".jpg");
imagejpeg($image5J, $filePath5 . ".jpg");
imagejpeg($image6J, $filePath6 . ".jpg");
unlink($filePath1 . ".jpeg"); unlink($filePath2 . ".jpeg"); unlink($filePath3 . ".jpeg"); unlink($filePath4 . ".jpeg"); unlink($filePath5 . ".jpeg"); unlink($filePath6 . ".jpeg");
