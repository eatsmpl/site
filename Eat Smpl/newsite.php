<?php
//**********Create directory**********//
$submissionID = mt_rand(00000,99999);
$phar = new PharData('tmp/sites.tar');
$phar->extractTo('tmp/done/' . $_POST["restaurant-name-input"] );

//**********Begin Yelp API Script**********//
//-----------------------------------------//
// Link to oauth php file
require_once('OAuth.php');
// Import API credentials
$CONSUMER_KEY = "8zM9IhHGBtKr6ySLJV45pA";
$CONSUMER_SECRET = "Rha-rPVvN4walIRh7cgCNO1SfWM";
$TOKEN = "O_zTuD-w4MgFHNpCiJJFpOLxvIrtBPPR";
$TOKEN_SECRET = "DU8VOZF8_L7llr2eU8je1HV1Y3Q";

$API_HOST = 'api.yelp.com';
$DEFAULT_TERM = '';
$DEFAULT_LOCATION = '';
$SEARCH_LIMIT = 3;
$SEARCH_PATH = '/v2/search/';
$BUSINESS_PATH = '/v2/business/';

/**
 * Makes a request to the Yelp API and returns the response
 *
 * @param    $host    The domain host of the API
 * @param    $path    The path of the APi after the domain
 * @return   The JSON response from the request
 */
function request($host, $path) {
    $unsigned_url = "http://" . $host . $path;

    // Token object built using the OAuth library
    $token = new OAuthToken($GLOBALS['TOKEN'], $GLOBALS['TOKEN_SECRET']);

    // Consumer object built using the OAuth library
    $consumer = new OAuthConsumer($GLOBALS['CONSUMER_KEY'], $GLOBALS['CONSUMER_SECRET']);

    // Yelp uses HMAC SHA1 encoding
    $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

    $oauthrequest = OAuthRequest::from_consumer_and_token(
        $consumer,
        $token,
        'GET',
        $unsigned_url
    );

    // Sign the request
    $oauthrequest->sign_request($signature_method, $consumer, $token);

    // Get the signed URL
    $signed_url = $oauthrequest->to_url();

    // Send Yelp API Call
    $ch = curl_init($signed_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

/**
 * Query the Search API by a search term and location
 *
 * @param    $term        The search term passed to the API
 * @param    $location    The search location passed to the API
 * @return   The JSON response from the request
 */
function search($term, $location) {
    $url_params = array();

    $url_params['term'] = $term ?: $GLOBALS['DEFAULT_TERM'];
    $url_params['location'] = $location?: $GLOBALS['DEFAULT_LOCATION'];
    $url_params['limit'] = $GLOBALS['SEARCH_LIMIT'];
    $search_path = $GLOBALS['SEARCH_PATH'] . "?" . http_build_query($url_params);

    return request($GLOBALS['API_HOST'], $search_path);
}

/**
 * Query the Business API by business_id
 *
 * @param    $business_id    The ID of the business to query
 * @return   The JSON response from the request
 */
function get_business($business_id) {
    $business_path = $GLOBALS['BUSINESS_PATH'] . $business_id;

    return request($GLOBALS['API_HOST'], $business_path);
}

/**
 * Queries the API by the input values from the user
 *
 * @param    $term        The search term to query
 * @param    $location    The location of the business to query
 */
function query_api($term, $location) {
    $response = json_decode(search($term, $location));
    $business_id = $response->businesses[0]->id;

/*    print sprintf(
        "%d businesses found, querying business info for the top result \"%s\"\n\n",
        count($response->businesses),
        $business_id
    ); */

    $response = get_business($business_id);

  /*  print sprintf("Result for business \"%s\" found:\n", $business_id); */
    print "$response";
    file_put_contents('tmp/done/' . $_POST["restaurant-name-input"] . '/sites' . '/yelp.json', $response);
}

/**
 * User input is handled here
 */
$longopts  = array(
    "term::",
    "location::",
);

$options = getopt("", $longopts);

$term = $options['term'] ?: $_POST["restaurant-name-input"];
$location = $options['location'] ?: $_POST["restaurant-city-input"];

query_api($term, $location);

/* *************** Import JSON files *************** */
$yelpString = file_get_contents('tmp/done/' . $_POST["restaurant-name-input"] . '/sites' . '/yelp.json');
$yelpJson = json_decode($yelpString, true);
/*echo '<pre>' . print_r($yelpJson, true) . '</pre>';
echo $yelpJson['name']; */

$formString = file_get_contents('tmp/done/' . $_POST["restaurant-name-input"] . '/sites' . '/mydata.json');
$formJson = json_decode($formString, true);
$formJson['restaurant']['name'] = $yelpJson['name'];
$formJson['restaurant']['address'][0]['streetAddress'] = $yelpJson['location']['display_address'][0] . ' ' . $yelpJson['location']['display_address'][1];
$formJson['restaurant']['address'][0]['city'] = $yelpJson['location']['city'];
$formJson['restaurant']['address'][0]['state'] = $yelpJson['location']['state_code'];
$formJson['restaurant']['address'][0]['postalCode'] = $yelpJson['location']['postal_code'];

/*echo '<pre>' . print_r($formJson, true) . '</pre>'; */



//**********Begin Google API Script**********//
//-------------------------------------------//

//**********Declare global variables**********
$KEY='&key=AIzaSyDj8wxtxu3eXqMdiB-CDpO-h21cz05MLfA';
$NAME= $yelpJson['name'];
$NAME= preg_replace("/[\s_]/", "-", $NAME);
$ADDRESS= $yelpJson['location']['display_address'][0];
$ADDRESS= preg_replace("/[\s_]/", "-", $ADDRESS);
$URL= 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=';


//**********Search through API for the restaurants ID**********
$searchJson = file_get_contents($URL.$NAME.$ADDRESS.$KEY);
$searchJson = json_decode($searchJson, true);

foreach ($searchJson['results'] as $place_id)
{
	$googleID = $place_id['place_id'];
};

//**********Get restaurant details from ID**********
$URL= 'https://maps.googleapis.com/maps/api/place/details/json?placeid=' . $googleID . $KEY;
// Curl JSON data
$ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $URL);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $restaurantInfo = json_decode(curl_exec($ch), true);

//**********Start review filter script**********
//Grab review text
for ($i = 0; $i <= 4; $i++) {
	$reviewText[$i]= $restaurantInfo['result']['reviews'][$i]['text'];
}
//Grab review rating
for ($i = 0; $i <= 4; $i++) {
	$reviewRating[$i]= $restaurantInfo['result']['reviews'][$i]['rating'];
}
//Grab review source
for ($i = 0; $i <= 4; $i++) {
	$reviewSource[$i]= $restaurantInfo['result']['reviews'][$i]['author_name'];
}

//Filter through content for only good reviews (4-5 Stars)
$goodReviewCount = 0;
for ($i = 0; $i <= 4; $i++) {

	if ($reviewRating[$i] >= 4) {
		$goodReviewRating[$i] = $reviewRating[$i];
		$goodReviewText[$i] = $reviewText[$i];
		$goodReviewSource[$i] = $reviewSource[$i];
		$goodReviewCount++;
	}

}

//Get lengths of each review
$lengths = array_map('strlen', $goodReviewText);

// Find shortest review
$shortest = min($lengths);
for ($i=1; $i < $goodReviewCount+1; $i++) {
	if (strlen($goodReviewText[$i]) == $shortest) {
  		$selectedReviewText = $goodReviewText[$i];
  		$selectedReviewSource = $goodReviewSource[$i];
	}
}

//**********Grab some more useful data from the JSON array**********
// Convert some collected data to variables
$lat= $restaurantInfo['result']['geometry']['location']['lat'];
$lng= $restaurantInfo['result']['geometry']['location']['lng'];
$mon= $restaurantInfo['result']['opening_hours']['weekday_text'][0];
$tue= $restaurantInfo['result']['opening_hours']['weekday_text'][1];
$wed= $restaurantInfo['result']['opening_hours']['weekday_text'][2];
$thu= $restaurantInfo['result']['opening_hours']['weekday_text'][3];
$fri= $restaurantInfo['result']['opening_hours']['weekday_text'][4];
$sat= $restaurantInfo['result']['opening_hours']['weekday_text'][5];
$sun= $restaurantInfo['result']['opening_hours']['weekday_text'][6];
$mon= substr($mon, 8);
$tue= substr($tue, 9);
$wed= substr($wed, 11);
$thu= substr($thu, 10);
$fri= substr($fri, 8);
$sat= substr($sat, 10);
$sun= substr($sun, 8);
$formJson['restaurant']['address'][0]['lat'] = $lat;
$formJson['restaurant']['address'][0]['lng'] = $lng;
$formJson['restaurant']['aboutInfo']['quote'] = "\"" . "$selectedReviewText" . "\"";
$formJson['restaurant']['aboutInfo']['quoteCitation'] = "$selectedReviewSource";
$formJson['restaurant']['phoneNumber'] = $restaurantInfo['result']['formatted_phone_number'];
$formJson['restaurant']['hours'][0]['openHours'] = $sun;
$formJson['restaurant']['hours'][1]['openHours'] = $mon;
$formJson['restaurant']['hours'][2]['openHours'] = $tue;
$formJson['restaurant']['hours'][3]['openHours'] = $wed;
$formJson['restaurant']['hours'][4]['openHours'] = $thu;
$formJson['restaurant']['hours'][5]['openHours'] = $fri;
$formJson['restaurant']['hours'][6]['openHours'] = $sat;
$formJsonAfter = json_encode($formJson);
file_put_contents('tmp/done/' . $_POST["restaurant-name-input"] . '/sites' . '/mydata.json', $formJsonAfter);
/* *************** Redirect to new directory *************** */
unlink('tmp/done/' . $_POST["restaurant-name-input"] . '/sites' . '/yelp.json');
header("Location: /tmp/done/" . $_POST["restaurant-name-input"] . "/sites/step1");
