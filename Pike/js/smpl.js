$.getJSON("../mydata.json", function (data) {
//JSON START

//HTML syntax assign
    var syntax = {
        "spanOpen": "<span>",
        "spanClose": "</span>",
        "hr": "<hr>",
        "h3Open": "<h3>",
        "h3Close": "</h3>",
        "pOpen": "<p>",
        "pClose": "</p>",
        "ahrefOpen": "<a href='",
        "ahrefOpenEnd": "'>",
        "ahrefClose": "</a>",
        "pike": {
            "menu": {
                "briefOpen": "<p class='menu-brief'>",
                "briefClose": "</p>",
                "legendOpen": "<p class='legend'>",
                "legendClose": "</p>",
                "legendIndicatorOpen": "<span class='legend-indicator'>",
                "legendIndicatorClose": "</span>",
                "legendIndicatorSpacer": " - ",
                "categoriesOpen": "<p class='menu-category'>",
                "categoriesClose": "</p>"
            },
            "socialIconSpanOpen": "<span class='symbol restaurant-socialIcon'>",
            "socialIconSpanClose": "</span>"
        }
    };
// JSON END

// JS BEGIN

//Menu Generator START
//Legend
    function createMenuLegend(name) {
        var toReturn = '';
        if (typeof data.menuUserInput[name] !== 'undefined') {
            for (ind in data.menuUserInput[name]) {
                toReturn += syntax.pike.menu.legendIndicatorOpen + data.menuUserInput[name][ind].symbol + syntax.pike.menu.legendIndicatorClose + syntax.pike.menu.legendIndicatorSpacer + data.menuUserInput[name][ind].description + ", ";
            }
        }
        return toReturn;
    }

//Menu Entries + Categories
    function createMenuEntries(name) {
        var toReturn = '';
        if (typeof data.menuUserInput[name] !== 'undefined') {
            for (ind in data.menuUserInput[name]) {
            toReturn += syntax.pike.menu.categoriesOpen + data.menuUserInput[name][ind].category + syntax.pike.menu.categoriesClose + syntax.h3Open + data.menuUserInput[name][ind].title + " " + syntax.pike.menu.legendIndicatorOpen + data.menuUserInput[name][ind].indicators + syntax.pike.menu.legendIndicatorClose + syntax.h3Close + syntax.pOpen + data.menuUserInput[name][ind].description + syntax.pClose;  
            }
        }
        return toReturn;
    }

// Abstracted Menu Items
    var menuItems = {
        "header": "<h1>Menu</h1>",
        "brief": syntax.pike.menu.briefOpen + data.menuUserInput.brief + syntax.pike.menu.briefClose,
        "legend": syntax.pike.menu.legendOpen + createMenuLegend('legend') + syntax.pike.menu.legendClose,
        "entries": createMenuEntries('entries')
    };

// Finished Menu Generation
    var menuComplete = {
        "menu": menuItems.header + menuItems.brief + syntax.hr + menuItems.legend + syntax.hr + menuItems.entries
    };

// Social Icons

    function createSocialIcons(name) {
        var toReturn = '';
        if (typeof data.restaurant[name] !== 'undefined') {
            for (ind in data.restaurant[name]) {
                toReturn += syntax.ahrefOpen + data.restaurant[name][ind].link + syntax.ahrefOpenEnd + syntax.pike.socialIconSpanOpen + data.restaurant[name][ind].icon + syntax.pike.socialIconSpanClose + syntax.ahrefClose;
            }
        }
        return toReturn;
    }

    var socialIcons = createSocialIcons('social');

// Convert everything to HTML
    $(document).ready(function () {
        $('.restaurant-name').html(data.restaurant.name);
        $('.restaurant-slogan').html(data.restaurant.slogan);
        $('.restaurant-quote').html(data.restaurant.quote);
        $('.restaurant-quoteCitation').html(data.restaurant.quoteCitation);
        $('.restaurant-streetAddress').html(data.restaurant.address.streetAddress);
        $('.restaurant-city').html(data.restaurant.address.city);
        $('.restaurant-state').html(data.restaurant.address.state);
        $('.restaurant-postalCode').html(data.restaurant.address.postalCode);
        $('.restaurant-phoneNumber').html(data.restaurant.phoneNumber);
        $('.restaurant-contactEmailLink').attr("href", "mailto:" + data.restaurant.emailAddress);
        $('.restaurant-contactEmail').html(data.restaurant.emailAddress);
        $('.restaurant-hours-MONTHUR').html(data.restaurant.hours.MONTHUR);
        $('.restaurant-hours-FRISAT').html(data.restaurant.hours.FRISAT);
        $('.restaurant-hours-SUN').html(data.restaurant.hours.SUN);
        $('.restaurant-aboutBio').html(data.restaurant.aboutBio);
        $('.restaurant-menu').html(menuComplete.menu);
        $('.restaurant-menu').html(menuComplete.menu);
        $('.restaurant-contactForm').attr("action", "////formspree.io/" + data.restaurant.contactEmail);
        $('.restaurant-footer').html(data.restaurant.footer.message + " " + data.restaurant.footer.copyright);
        $('.restaurant-socialLink').html(null);
        $('.restaurant-socialIcon').html(socialIcons);
    });
});